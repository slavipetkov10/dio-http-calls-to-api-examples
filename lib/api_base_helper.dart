import 'package:dio/dio.dart';

class ApiBaseHelper {
  static final String url = 'http://xprmnt.xyz:7777/api/User/';
  static BaseOptions opts = BaseOptions(
    baseUrl: url,
    responseType: ResponseType.json,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  );

  static Dio createDio() {
    return Dio(opts);
  }

  static Dio addInterceptors(Dio dio) {
    return dio
      ..interceptors.add(
        InterceptorsWrapper(
            onRequest: (RequestOptions options) => requestInterceptor(options),
            onError: (DioError e) async {
              print('DioError : ' + e.toString());
              print('DioError1 : ' + e.error.toString());
              return e.response.data;
            }),
      );
  }

  static dynamic requestInterceptor(RequestOptions options) async {
    // Get your JWT token
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1MjQ4NzQ0OC01NWU2LTQyNDctOTU4ZS0zOGQ0YzQ0YmVkNjIiLCJhdWQiOlsieHBybW50Lnh5eiIsInhwcm1udC54eXoiXSwianRpIjoiZDM0ZDRkMzMtMzBkOC00ZGU4LTgxY2ItNGY2NmQyYTIwOTQzIiwiaWF0IjoiMTYxMTIxNzU2MyIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IlBob3RvZ3JhcGhlciIsImV4cCI6MTYxMTIyNDc2MywiaXNzIjoieHBybW50Lnh5eiJ9.v2NBgrPSEY0-qBSibwoS2DpmEOm8wcdfONGianeMuxQ';
    options.headers.addAll({"Authorization": "Bearer: $token"});
    return options;
  }

  static final dio = createDio();
  static final baseAPI = addInterceptors(dio);

  Future<Response> getHTTP(String url) async {
    try {
      Response response = await baseAPI.get(url);
      print('getHTTP: ' + response.toString());
      return response;
    } on DioError catch(e) {
      // Handle error
    }
  }

  Future<Response> postHTTP(String url, dynamic data) async {
    try {
      Response response = await baseAPI.post(url, data: data);
      return response;
    } on DioError catch(e) {
      // Handle error
    }
  }

  Future<Response> putHTTP(String url, dynamic data) async {
    try {
      Response response = await baseAPI.put(url, data: data);
      return response;
    } on DioError catch(e) {
      // Handle error
    }
  }

  Future<Response> deleteHTTP(String url) async {
    try {
      Response response = await baseAPI.delete(url);
      return response;
    } on DioError catch(e) {
      // Handle error
    }
  }
}