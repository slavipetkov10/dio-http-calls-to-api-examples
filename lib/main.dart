import 'package:dio/dio.dart';
import 'package:dio_http_calls_to_api_example/api_base_helper.dart';
import 'package:flutter/material.dart';

void main() {
  var myApp = MyApp();
  myApp.displayStatusCode();
}

class MyApp  {
  MyApp();
  final userId = '52487448-55e6-4247-958e-38d4c44bed62';
  void displayStatusCode() async {

    var apiBaseHelper = ApiBaseHelper();
    Response response = await apiBaseHelper.getHTTP(userId);
    print('BaseApi : '+ response.toString());
  }
}
